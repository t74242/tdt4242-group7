from django.test import TestCase, Client
from rest_framework.reverse import reverse
from allpairspy import AllPairs
from workouts.models import Workout, Exercise, ExerciseInstance
from users.models import User
from rest_framework.test import APITestCase, APIRequestFactory, force_authenticate
from rest_framework.request import Request
from workouts.views import WorkoutDetail
from datetime import date
from django.db import models


class TwoWayDomain(TestCase):
    def setUp(self):
        data = {
            "username": "user",
            "email": "mail@mail.com",
            "password": "test",
            "password1": "test",
            "phone_number": "12345678",
            "country": "testland",
            "city": "test city",
            "street_address": "test avenue",
        }

        resp = self.client.post(reverse("user-list"), data)

    def test_register_ue(self):
        parameters = [["", "username", "user"], ["mail", "mail@mail.com"]]

        for i in list(AllPairs(parameters, n=2)):
            if i[0] == "" or i[0] == "user" or i[1] == "mail":
                code = 400
            else:
                code = 201
            data = {
                "username": i[0],
                "email": i[1],
                "password": "test",
                "password1": "test",
                "phone_number": "12345678",
                "country": "testland",
                "city": "test city",
                "street_address": "test avenue",
            }

            resp = self.client.post(reverse("user-list"), data)
            self.assertEqual(resp.status_code, code)

    def test_register_up(self):
        parameters = [["", "username", "user"], ["", "password"]]

        for i in list(AllPairs(parameters, n=2)):
            if i[0] == "" or i[0] == "user" or i[1] == "":
                code = 400
            else:
                code = 201
            data = {
                "username": i[0],
                "email": "mail@mail.com",
                "password": i[1],
                "password1": "password",
                "phone_number": "12345678",
                "country": "testland",
                "city": "test city",
                "street_address": "test avenue",
            }
            resp = self.client.post(reverse("user-list"), data)
            self.assertEqual(resp.status_code, code)

    def test_register_upn(self):
        parameters = [["", "username", "user"], ["", "password"]]

        for i in list(AllPairs(parameters, n=2)):
            if i[0] == "" or i[0] == "user" or i[1] == "":
                code = 400
            else:
                code = 201
            data = {
                "username": i[0],
                "email": "mail@mail.com",
                "password": "password",
                "password1": i[1],
                "phone_number": "12345678",
                "country": "testland",
                "city": "test city",
                "street_address": "test avenue",
            }
            resp = self.client.post(reverse("user-list"), data)
            self.assertEqual(resp.status_code, code)

    def test_register_epn(self):
        parameters = [["mail", "mail@mail.com"], ["", "password"]]

        for i in list(AllPairs(parameters, n=2)):
            if i[0] == "mail" or i[1] == "":
                code = 400
            else:
                code = 201
            data = {
                "username": "user1",
                "email": i[0],
                "password": i[1],
                "password1": "password",
                "phone_number": "12345678",
                "country": "testland",
                "city": "test city",
                "street_address": "test avenue",
            }
            resp = self.client.post(reverse("user-list"), data)
            self.assertEqual(resp.status_code, code)

    def test_register_ep(self):
        parameters = [["mail", "mail@mail.com"], ["", "password"]]

        for i in list(AllPairs(parameters, n=2)):
            if i[0] == "mail" or i[1] == "":
                code = 400
            else:
                code = 201
            data = {
                "username": "user1",
                "email": i[0],
                "password": "password",
                "password1": i[1],
                "phone_number": "12345678",
                "country": "testland",
                "city": "test city",
                "street_address": "test avenue",
            }
            resp = self.client.post(reverse("user-list"), data)
            self.assertEqual(resp.status_code, code)

    def test_register_ppn(self):
        parameters = [["", "password"], ["", "password"]]

        for i in list(AllPairs(parameters, n=2)):
            if i[0] == "" or i[1] == "":
                code = 400
            else:
                code = 201
            data = {
                "username": "username3",
                "email": "mail@mail.com",
                "password": i[0],
                "password1": i[1],
                "phone_number": "12345678",
                "country": "testland",
                "city": "test city",
                "street_address": "test avenue",
            }
            resp = self.client.post(reverse("user-list"), data)
            self.assertEqual(resp.status_code, code)


class BoundaryTesting(TestCase):
    def test_register_ue(self):
        parameters = [["", "username"], ["mail", "mail@mail.com"]]

        for i in list(AllPairs(parameters, n=2)):
            if i[0] == "" or i[1] == "mail":
                code = 400
            else:
                code = 201
            data = {
                "username": i[0],
                "email": i[1],
                "password": "test",
                "password1": "test",
                "phone_number": "12345678",
                "country": "testland",
                "city": "test city",
                "street_address": "test avenue",
            }

            resp = self.client.post(reverse("user-list"), data)
            self.assertEqual(resp.status_code, code)

    def test_register_up(self):
        parameters = [["", "username"], ["", "password"]]

        for i in list(AllPairs(parameters, n=2)):
            if i[0] == "" or i[1] == "":
                code = 400
            else:
                code = 201
            data = {
                "username": i[0],
                "email": "mail@mail.com",
                "password": i[1],
                "password1": "password",
                "phone_number": "12345678",
                "country": "testland",
                "city": "test city",
                "street_address": "test avenue",
            }
            resp = self.client.post(reverse("user-list"), data)
            self.assertEqual(resp.status_code, code)

    def test_register_upn(self):
        parameters = [["", "username"], ["", "password"]]

        for i in list(AllPairs(parameters, n=2)):
            if i[0] == "" or i[1] == "":
                code = 400
            else:
                code = 201
            data = {
                "username": i[0],
                "email": "mail@mail.com",
                "password": "password",
                "password1": i[1],
                "phone_number": "12345678",
                "country": "testland",
                "city": "test city",
                "street_address": "test avenue",
            }
            resp = self.client.post(reverse("user-list"), data)
            self.assertEqual(resp.status_code, code)

    def test_register_epn(self):
        parameters = [["mail", "mail@mail.com"], ["", "password"]]

        for i in list(AllPairs(parameters, n=2)):
            if i[0] == "mail" or i[1] == "":
                code = 400
            else:
                code = 201
            data = {
                "username": "user1",
                "email": i[0],
                "password": i[1],
                "password1": "password",
                "phone_number": "12345678",
                "country": "testland",
                "city": "test city",
                "street_address": "test avenue",
            }
            resp = self.client.post(reverse("user-list"), data)
            self.assertEqual(resp.status_code, code)

    def test_register_ep(self):
        parameters = [["mail", "mail@mail.com"], ["", "password"]]

        for i in list(AllPairs(parameters, n=2)):
            if i[0] == "mail" or i[1] == "":
                code = 400
            else:
                code = 201
            data = {
                "username": "user1",
                "email": i[0],
                "password": "password",
                "password1": i[1],
                "phone_number": "12345678",
                "country": "testland",
                "city": "test city",
                "street_address": "test avenue",
            }
            resp = self.client.post(reverse("user-list"), data)
            self.assertEqual(resp.status_code, code)

    def test_register_ppn(self):
        parameters = [["", "password"], ["", "password"]]

        for i in list(AllPairs(parameters, n=2)):
            if i[0] == "" or i[1] == "":
                code = 400
            else:
                code = 201
            data = {
                "username": "username3",
                "email": "mail@mail.com",
                "password": i[0],
                "password1": i[1],
                "phone_number": "12345678",
                "country": "testland",
                "city": "test city",
                "street_address": "test avenue",
            }
            resp = self.client.post(reverse("user-list"), data)
            self.assertEqual(resp.status_code, code)
