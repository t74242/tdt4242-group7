from django.test import TestCase
from rest_framework.reverse import reverse
from allpairspy import AllPairs
from rest_framework.test import APITestCase, APIRequestFactory
from rest_framework.request import Request
from .views import UserList


# Create your tests here.


class SerializerTest(TestCase):
    def test_create_user(self):
        data = {
            "username": "testuser",
            "email": "test@mail.no",
            "password": "test",
            "password1": "test",
            "phone_number": "12345678",
            "country": "testland",
            "city": "test city",
            "street_address": "test avenue",
        }
        resp = self.client.post(reverse("user-list"), data)
        print(resp)
        self.assertEqual(resp.status_code, 201)

    def test_fail_password_valid(self):
        data = {
            "username": "testuser1",
            "email": "test@mail.no",
            "password": "jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj",
            "password1": "testing",
            "phone_number": "12345678",
            "country": "testland",
            "city": "test city",
            "street_address": "test avenue",
        }
        resp = self.client.post(reverse("user-list"), data)
        print(resp)
        self.assertEqual(resp.status_code, 201)
