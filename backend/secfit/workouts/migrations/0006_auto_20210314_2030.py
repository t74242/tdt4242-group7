# Generated by Django 3.1 on 2021-03-14 20:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("workouts", "0005_auto_20210314_2016"),
    ]

    operations = [
        migrations.AlterField(
            model_name="workout",
            name="completion_date",
            field=models.DateTimeField(blank=True, default=None, null=True),
        ),
    ]
